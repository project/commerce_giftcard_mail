<?php

namespace Drupal\commerce_giftcard_mail\Plugin\Commerce\EmailEvent;

use Drupal\Component\EventDispatcher\Event;
use Drupal\commerce_email\Plugin\Commerce\EmailEvent\EmailEventBase;
use Drupal\commerce_giftcard\Event\GiftcardActivatedEvent;

/**
 * Provides the OrderPaid email event.
 *
 * @CommerceEmailEvent(
 *   id = "giftcard_activated",
 *   label = @Translation("Giftcard activated"),
 *   event_name = "commerce_giftcard.giftcard_activated",
 *   entity_type = "commerce_giftcard",
 * )
 */
class GiftcardActivated extends EmailEventBase {

  /**
   * {@inheritdoc}
   */
  public function extractEntityFromEvent(Event $event) {
    assert($event instanceof GiftcardActivatedEvent);
    return $event->getGiftcard();
  }

}
