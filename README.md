Module to fill the gap between Commerce Gift Card and Commerce Email.

It allows you to send E-mails with the Giftcard details to a customer 
when the order containing the giftcard has being paid.

INSTALLATION

Install the module

IMPORTANT: You need to install the patch on this issue: 
https://www.drupal.org/project/commerce_giftcard/issues/3181301 
which includes the 'commerce_giftcard.giftcard_activated' event, 
dispatched when the order has being paid.

CREATE AN E-MAIL

Go to the Commerce Email confiugration page: /admin/commerce/config/emails 
and create a new Email entity with the Giftcard activated event.
In the e-mail you can use the giftcard entity tokens.
